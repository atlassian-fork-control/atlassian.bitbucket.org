;(function($, Mustache, exports) {

    var CELLS_PER_ROW = 4;

    var filters = {
        '#jira' : function(repo) {
            return repo.slug.indexOf("jira") > -1
        },
        '#confluence' : function(repo) {
            return repo.slug.indexOf("confluence") > -1
        },
        '#bitbucket' : function(repo) {
            return (repo.slug.indexOf("stash") > -1 || repo.slug.indexOf("bitbucket") > -1)
        },
        '#bamboo' : function(repo) {
            return repo.slug.indexOf("bamboo") > -1
        },
        '#hipchat' : function(repo) {
            return repo.slug.indexOf("hipchat") > -1
        }
    };

    var repositories = [];

    function collectRepositories(repos, isLabs) {
        for (var i = 0, l = repos.length; i < l; i++) {
            var repo = repos[i];
            repo.is_labs = isLabs;
            // Only include public repositories with descriptions
            if (!repo.is_private && repo.description) {
                repositories.push(repo);
            }
        }
    }

    function refreshPage() {
        var $repos = $('#repositories'),
            reposHTML = [];

        reposHTML.push(drawReposForCategory({isLabs: false}));
        reposHTML.push(drawReposForCategory({isLabs: true}));

        $repos.html(reposHTML.join(''));
        // Make sure the descriptions aren't too long
        $repos.find('.repo .description').trunk8({
            lines: 8
        });
    }

    function drawReposForCategory(opts) {
        var reposByLang = {},
            categoryTemplate = $('#category-template').html(),
            langTemplate = $('#lang-template').html(),
            categoryHTML = [],
            productFilter = filters[location.hash],
            i, ii, j, jj, lang, langs, repo, repos, groupedRepos, filter, labsFilter;
        opts = opts || {};

        // We only show these languages, in order
        langs = [
            // Format: [repo.language, displayName]
            ['java', 'Java'],
            ['javascript', 'JavaScript'],
            ['nodejs', 'Node'],
            ['objective-c', 'Objective-C'],
            ['python', 'Python'],
            ['ruby', 'Ruby'],
            ['scala', 'Scala'],
        ];

        // Update filter to check for Labs if required
        filter = productFilter;
        if ('isLabs' in opts) {
            labsFilter = function (repo) {
                return repo.is_labs == opts.isLabs;
            };
            if (productFilter) {
                filter = function (repo) {
                    return productFilter(repo) && labsFilter(repo);
                };
            } else {
                filter = labsFilter;
            }
        }

        // Group repos by language
        for (i = 0, ii = repositories.length; i < ii; i++) {
            repo = repositories[i];
            if (filter && !filter(repo)) {
                continue;
            }
            lang = repo.language || 'other';
            reposByLang[lang] = reposByLang[lang] || [];
            reposByLang[lang].push(repo);
        }

        var previousInRow = 0;
        // Sort/group/display repos for each language
        for (i = 0, ii = langs.length; i < ii; i++) {
            lang = langs[i];
            repos = reposByLang[lang[0]];
            if (!repos) {
                continue;
            }

            // Sort by followers (most popular first), then alphabetical
            repos.sort(function (a, b) {
                var count = b.followers_count - a.followers_count;
                var name = a.name === b.name ? 0 : a.name < b.name ? -1 : 1;
                return count || name;
            });

            // Group repos into blocks of 4 for the template
            groupedRepos = [];
            for (j = 0, jj = repos.length; j < jj; j += CELLS_PER_ROW) {
                groupedRepos.push({repos: repos.slice(j, j + CELLS_PER_ROW)});
            }

            var compact = repos.length < CELLS_PER_ROW;
            if (compact && (previousInRow + repos.length > CELLS_PER_ROW)) {
                previousInRow = 0;
            }

            // Render template for language
            categoryHTML.push(Mustache.render(langTemplate, {
                lang: lang[1],
                groupedRepos: groupedRepos,
                compact: repos.length < CELLS_PER_ROW,
                compactStart: compact && previousInRow === 0
            }));
            if (compact) {
                previousInRow += repos.length;
                previousInRow = previousInRow % CELLS_PER_ROW;
            } else {
                previousInRow = 0;
            }
        }

        return Mustache.render(categoryTemplate, {
            isLabs: opts.isLabs,
            categoryMarkup: categoryHTML.join('')
        });
    }

    exports.collectSupportedRepositories = function(user) {
        collectRepositories(user.repositories || [], false);
    };
    exports.collectLabsRepositories = function(user) {
        collectRepositories(user.repositories || [], true);
    };
    exports.initPage = function() {
        refreshPage();
        window.onhashchange = refreshPage;
    };

    // init buttons
    $(document).on('click', '.aui-buttons > .aui-button', function(e) {
        $(e.target)
            .closest('.aui-buttons').find('.aui-button').attr('aria-pressed', false).end().end()
            .attr('aria-pressed', true);
    });
    $('.aui-buttons > .aui-button').each(function() {
        var $button = $(this);
        var hash = location.hash || '#';
        $button.attr('aria-pressed', $button.attr('href') === hash);
    });
})(jQuery, Mustache, this);
